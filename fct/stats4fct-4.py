# USE LIKE THIS python stats4.py 60pc-12us-100G OUTFILE
# input files number 60pc-12us-100G-1.csv, .. where the files have ..
# (time,idealFlowSynId), (time, idealFlowSize), (time, idealFlowFCT) , perhop (..), rcp (..) .. no sorting just export filter
# like attr:l(0.6) AND attr:j(10) AND attr:c(10e9) AND  (name(flowSynId:vector) OR name(flowSynSize:vector) OR name(flowCompletionTime:vector) )
# .. sort by expname before exporting to 60pc-12us-100G.csv

import fileinput
import os
import sys
EXPERIMENT=sys.argv[1] # "all-1.2us.csv"
OUTFILE= sys.argv[2]#"stats-1.2us.txt"

print EXPERIMENT
print OUTFILE

#bins = ["%d-%d"%((10**i)+1, 10**(i+1)) for i in range(3,8)]
bins = ["%d-%d"%(i,j) for i,j in [(1001,10000),(10001,1000000),(1000000,100000000)]]
#binNames = [str(sz) for sz in bins]
binNames = ["small", "medium", "large"]
print bins

skip = ["CHARNY_AGG"]
algs = ["CHARNY_AGG", "CHARNY_CONS", "DCTCP", "IDEAL", "PERC", "RCP"]
#algs = ["ideal","perc-c2","rcp"]
indalgs = {}#"ideal":0, "perhopa": 1, "perhopc": 2, "rcp": 3}
fctcols = {}
sizecols = {}
idcols = {}
i = 0
for alg in algs:
    indalgs[alg] = i
    fctcols[alg] = 6*indalgs[alg]+1+1
    sizecols[alg] = 6*indalgs[alg]+6-2+1+1
    idcols[alg] = 6*indalgs[alg]+3+1
    i+=1
    pass
print fctcols
print sizecols


loadStr,capStr,rttStr = EXPERIMENT.split("-")


cap = int(capStr.rstrip("G")) * 1.0e+9
rtt = float(rttStr[:-2]) * 1.0e-6
load = int(loadStr[:-2])/100.0

print "cap: %f, rtt: %f, load: %f" % (cap, rtt, load)

LSSTRING_OUTFILE = "tmp/%s_LSSTRING.txt"%OUTFILE
cmd = "ls -1 %s-*csv | sed \"s/\./-/g\" | sort -t\"-\" -n -k 4 | sed \"s/-csv/\.csv/g\" > %s" % (EXPERIMENT, LSSTRING_OUTFILE)
os.system(cmd)

INFILE =  "tmp/%s_INFILE_%s.txt"%(OUTFILE, EXPERIMENT)
cmd = "cat %s | xargs -n %d python newPaste.py %s > %s" % (LSSTRING_OUTFILE, len(algs)*3, EXPERIMENT, INFILE) 
print cmd
os.system(cmd)


cleanup = []
cleanup.append(cmd)
#print cmd



#cmd = "echo \"%s size in B\" > %s" % (str(bins), OUTFILE);
#print cmd
#os.system(cmd)

ideal_stats = {}
lineNum = 0
alg = "IDEAL"
print "Reading " + INFILE + " for IDEAL"
for line in fileinput.input(INFILE):
    if (lineNum == 0):
        lineNum += 1
        continue
    lineNum += 1
    words = line.rstrip().split(",")
    ideal_id = words[idcols[alg]-1]
    ideal_fct = words[fctcols[alg]-1]
    ideal_size = words[sizecols[alg]-1]
    if ideal_id not in ideal_stats:
        ideal_stats[ideal_id] = {"fct": ideal_fct, "size": ideal_size}
        pass
    else:
        print "Duplicated Flow ID in ideal column "\
            + ideal + " (line number: " + str(lineNum) + ")"
        pass
    pass


IDEALDC_LSSTRING_OUTFILE = "tmp/%s_IDEALDC_LSSTRING.txt"%OUTFILE
cmd = "ls -1 DCTCP/%s-IDEALDC*csv | sed \"s/\./-/g\" | sort -t\"-\" -n -k 4 | sed \"s/-csv/\.csv/g\" > %s" % (EXPERIMENT, IDEALDC_LSSTRING_OUTFILE)
print cmd
os.system(cmd)

IDEALDC_INFILE =  "tmp/%s_IDEALDC_INFILE_%s.txt"%(OUTFILE, EXPERIMENT)
cmd = "cat %s | xargs -n %d python newPaste.py %s > %s" % (IDEALDC_LSSTRING_OUTFILE, 3, EXPERIMENT, IDEALDC_INFILE) 
print cmd
os.system(cmd)


# Ideal DC order id, size, fct
ideal_dctcp_stats = {}
lineNum = 0
alg = "IDEAL_DCTCP"
print "Reading " + IDEALDC_INFILE + " for IDEAL_DCTCP"
for line in fileinput.input(IDEALDC_INFILE):
    if (lineNum == 0):
        lineNum += 1
        continue
    lineNum += 1
    words = line.rstrip().split(",")
    ideal_dctcp_id = words[3]
    ideal_dctcp_fct = words[1]
    ideal_dctcp_size = words[5]
    if ideal_dctcp_id not in ideal_dctcp_stats:
        ideal_dctcp_stats[ideal_dctcp_id] = {"fct": ideal_dctcp_fct, "size": ideal_dctcp_size}
        pass
    else:
        print "Duplicated Flow ID in ideal_dctcp column "\
            + ideal_dctcp_id + " (line number: " + str(lineNum) + ")"
        pass
    pass

fileinput.close()

allFiles = []
binindex = 0
for bin in bins:
    binindex += 1
    bin1, bin2 = [int(b) for b in bin.split("-")]
    for name in algs:
        if name in skip:
            continue
        ALG_BIN_OUTFILE = "tmp/%s_BIN_%s_ALG_%s" % (OUTFILE, bin, name)
        allFiles.append(ALG_BIN_OUTFILE)
        outfile = open(ALG_BIN_OUTFILE, "w")
        lineNum = 0
        print "Writing to file " + ALG_BIN_OUTFILE
        # 3_large_CHARNY_CONS_stretch
        outfile.write("%d_%s_%s_norm_fct\n"%(binindex, binNames[binindex-1], name))
        print "Reading " + INFILE

        for line in fileinput.input(INFILE):
            if lineNum == 0:
                lineNum += 1
                continue
            lineNum += 1

            words = line.rstrip().split(",")

            try:
                alg_id = words[idcols[name]-1]
                alg_fct = words[fctcols[name]-1]
                alg_size = words[sizecols[name]-1]
            except:
                print "Error parsing id, fct, size from line " + line.rstrip()
                continue
            size = float(alg_size)

            if lineNum < 0:
                print "Parsed id " + alg_id + ", fct " + alg_fct + ", size " + alg_size\
                    + " from line " + line.rstrip() + " and words " + str(words)\
                    + " for ideal (alg: " + alg + ", "\
                    + "line: " + str(lineNum)\
                    + " in " + INFILE + ")"
                pass

            
            if name == "DCTCP":
                if alg_id not in ideal_dctcp_stats:
                    print "No corresponding Flow ID " + alg_id\
                        + " for ideal (alg: " + alg + ", "\
                        + "line: " + str(lineNum)\
                        + " in " + INFILE + ")"
                    continue
                else:
                    ideal_fct = ideal_dctcp_stats[alg_id]["fct"]
                    ideal_size = ideal_dctcp_stats[alg_id]["size"]
                    pass
                pass
            else:
                if alg_id not in ideal_stats:
                    print "No corresponding Flow ID " + alg_id\
                        + " for ideal (alg: " + alg + ", "\
                        + "line: " + str(lineNum)\
                        + " in " + INFILE + ")"
                    continue
                else:
                    ideal_fct = ideal_stats[alg_id]["fct"]
                    ideal_size = ideal_stats[alg_id]["size"]
                    pass
                pass

            if not float(ideal_size) == float(alg_size):
                print "Different sizes for flow ID " + alg_id\
                    + " alg has " + alg_size + " vs ideal has " + ideal_size\
                    + " (alg: " + alg + ", line: " + str(lineNum)\
                    + " in " + INFILE + ")"
                continue
            if float(ideal_fct) == 0:
                print "Ideal FCT is 0, can't normalize for flow ID " + alg_id\
                    + " alg has " + alg_fct + " vs ideal has " + ideal_fct\
                    + " (alg: " + alg + ", line: " + str(lineNum)\
                    + " in " + INFILE + ")"
                continue
            if (size * 1500 >= bin1 and size * 1500 <= bin2):
                outfile.write("%f\n" % (float(alg_fct)/float(ideal_fct)))
                pass
            pass # for line in fileinput.input()
        fileinput.close()
        outfile.close()
        pass # for name in algs
    pass # for bin in bins

# the outer loop iterates through bins
# the inner loop iterates through different schemes
# so we make #BINS x #SCHEMES passes through @INFILE
# which has (check) x, fct, x, x, size for
# actually time, fct, time, id, time, size
# each of algs charny_agg, charny_cons, dctcp, ideal,
# perc, rcp
# indexing for cols starts at 1 because awk

# what we want to do - first iterate through file once to
# get ideal fcts per flow id
# then in this iteration, lookup ideal fct and normalize

allFilesStr = " ".join(filename for filename in allFiles)
ALL_OUTFILE = "tmp/%s_all-%s.txt"%(OUTFILE, EXPERIMENT)

PERCENTILES_OUTFILE = "tmp/%s_PERCENTILES-%s.txt"%(OUTFILE, EXPERIMENT)
cmd = "python paste_data.py %s %s > %s" % (OUTFILE, allFilesStr, ALL_OUTFILE)
    #print cmd
os.system(cmd)

        
rscriptStr = "Rscript bn-4.R"

cmd = "cat %s > tmp/%s_BOXPLOT_INPUT_%s.txt" % (ALL_OUTFILE, OUTFILE, EXPERIMENT)
os.system(cmd)
    
cmd = "cat %s | Rscript bn-4.R > %s" % (ALL_OUTFILE, PERCENTILES_OUTFILE)
os.system(cmd)

cmd = "mv boxplot-tailsandmean out/%s_TAILSANDMEAN_%s.pdf" % (OUTFILE, EXPERIMENT)
print cmd
os.system(cmd)

    
allFilesLbStr = "\\n".join(filename for filename in allFiles)
cmd = "printf \"%s\" | xargs rm" % (allFilesLbStr)
#print cmd
#os.system(cmd)

funq <- function(x) quantile(x, na.rm=TRUE, probs=c(0.5, 0.99))

for (cap in c(10,100)) 
{
    charny <- read.csv(sprintf("%dG-12us-charny.csv", cap))
    rcp1 <- read.csv(sprintf("%dG-12us-rcp.csv", cap))
    perhop <- read.csv(sprintf("%dG-12us-perhop.csv", cap))
    colnames(rcp1) <- c("rtts")
    rcp <- rcp1 #subset(rcp1, rtts <= 100)

    cdf <- mapply(funq, charny)
    sprintf("for %d Gb/s charny tail (50, 99)", cap)
    cdf

    cdf <- mapply(funq, perhop)
    sprintf("for %d Gb/s perhop tail (50, 99)", cap)
    cdf

    cdf <- mapply(funq, rcp)
    sprintf("for %d Gb/s rcp tail (50, 99)", cap)
    cdf 

    pdf(sprintf("CT-%dG-12us-cdfs.pdf", cap), height=4, width=6)
    par(mar=c(2.4,2.2,0,0)+0.1)
    colors <- c("dark green", "green", "red")
    plot(ecdf(charny[,1]), col=colors[1], main="", xlab="", ylab="", axes=FALSE, xlim=c(0,100), ylim=c(0,1), verticals=TRUE, do.p=FALSE)



    xticks <- c(0, 0.2, 0.4, 0.5, 0.6, 0.8, 0.99)
    axis(pos = -0.1, side=2, xticks, cex=0.9)

    yticks <- c(seq(from=0, to=20, by=10), seq(from=20, to=100, by=20))
    axis(pos = -0.02, side=1, yticks)

    
    abline(h=0.5, lty='dotted')
    abline(h=0.99, lty='dotted')
    #sprintf("RTTs to converge (%dG, 12us)", cap)
    title(xlab="RTTs to converge", ylab="CDF", line=1.4)
    plot(ecdf(perhop[,1]), col=colors[2], add=T, verticals=TRUE, do.p=FALSE)

    plot(ecdf(rcp[,1]), col=colors[3], add=T, verticals=TRUE, do.p=FALSE)


    # note flipped usual order
    legend(x=16, y=0.4, legend=c("PERC","CHARNY", "RCP"), fill=c("green", "dark green", "red"))
}

